package unifor.nati.arduinon;

import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_infos)
public class Infos extends AppCompatActivity {

    @ViewById(R.id.info)
    TextView infos;

    Thread updateThread;

    @AfterViews
    public void update(){

        updateThread = new Thread(){
            public void run(){
                while (true){
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            infos.setText(Handle_Singleton.getInstance().getWriteMessage());
                        }
                    });

                }
            }
        };
        updateThread.start();
    }

//    NotificationCompat.Builder mBuilder;
//
//    NotificationManager mNotificationManager;
//
//    @AfterViews
//    public void change(){
//
////        mBuilder = new NotificationCompat.Builder(this)
////                .setSmallIcon(R.drawable.ic_action_info)
////                .setContentTitle("Arduino Falando")
////                .setContentText("To vivo");
////
////        Intent resultIntent = new Intent(this, this.getClass());
////
////        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
////
////        stackBuilder.addParentStack(this.getClass());
////        stackBuilder.addNextIntent(resultIntent);
////
////        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
////
////        mBuilder.setContentIntent(resultPendingIntent);
////        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//
//
//
//        changeThread = new Thread(){
//
//            public void run(){
//
//                int oldBytes = 0;
//
//                while (true){
//                    try {
//                        Thread.sleep(500);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//
//                    if (oldBytes != Handle_Singleton.getInstance().getBytes()){
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                //infos.setText(Handle_Singleton.getInstance().getBytes() + "");
//                                //infos.setText(Handle_Singleton.getInstance().getMsg());
//
//                            }
//                        });
//
//                        //mNotificationManager.notify(getTaskId(), mBuilder.build());
//
//                    }
//                }
//            }
//        };
//        changeThread.start();
//    }

}

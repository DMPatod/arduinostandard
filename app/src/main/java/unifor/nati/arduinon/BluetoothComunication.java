package unifor.nati.arduinon;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import org.androidannotations.annotations.EService;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.UUID;

/**
 * Created by labm4 on 26/01/2016.
 */

@EService
public class BluetoothComunication extends Service {

    private static final String myUUID = "00001101-0000-1000-8000-00805f9b34fb";

    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothSocket bluetoothSocket;
    private BluetoothDevice bluetoothDevice;
    private String deviceAdress;

    private OutputStream outputStream;
    private InputStream inputStream;

    private Thread serviceThread;

    private Handler mHandler;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        mHandler = Handle_Singleton.getInstance().getHandler();

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        deviceAdress = (String) intent.getSerializableExtra("deviceAdress");

        try {
            startConnection(deviceAdress);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return super.onStartCommand(intent, flags, startId);
    }

    private void startConnection(String remoteDevice) throws IOException {
        Log.d("Service","startConnection");

        bluetoothDevice = mBluetoothAdapter.getRemoteDevice(remoteDevice);
        if (bluetoothDevice == null) {
            fallback();
        }

        startArduinoConnection();
    }

    private void startArduinoConnection() throws IOException {
        Log.d("Service", "startArduinoConnection");

        serviceThread = new Thread(new Runnable() {
            @Override
            public void run() {
                mBluetoothAdapter.cancelDiscovery();

                try {
                    Log.d("Service", "creating socket");
                    bluetoothSocket = bluetoothDevice.createRfcommSocketToServiceRecord(UUID.fromString(myUUID));
                } catch (IOException e) {
                    Log.d("Service", "fudeu");
                    e.printStackTrace();
                }

                Log.d("Service", "socket Created");

                try {
                    Log.d("Service", "Trying to connect Socket");
                    bluetoothSocket.connect();
                } catch (IOException e) {
                    Log.d("Service", "Fudeu");
                    e.printStackTrace();

                    try {
                        Log.d("Service", "FallBack");

                        Method m = bluetoothDevice.getClass().getMethod("createRfcommSocket", int.class);
                        bluetoothSocket = (BluetoothSocket) m.invoke(bluetoothDevice, 1);
                        bluetoothSocket.connect();
                    } catch (Exception e1) {
                        e1.printStackTrace();
                        try {
                            bluetoothSocket.close();
                        } catch (IOException e2) {
                            Log.d("Service", "Fudeu2");

                            e1.printStackTrace();
                        }
                    }
                }

                if(bluetoothSocket.isConnected()){
                    Log.d("Service", "yey");
                }


                try{
                    outputStream = bluetoothSocket.getOutputStream();
                    inputStream = bluetoothSocket.getInputStream();
                    Log.d("Service", "yey2");

                } catch (IOException e) {

                    e.printStackTrace();
                }

                readInputStream();

            }
        });
        serviceThread.start();
    }

    public void readInputStream(){
        Log.d("READ", "Begin");

        final int BUFFER_SIZE = 1024;
        byte[] buffer = new byte[BUFFER_SIZE];
        int bytes = 0;
        int begin = 0;

        while (true){
            try{
                bytes += inputStream.read(buffer, bytes, buffer.length - bytes);
                for(int i = begin; i < bytes; i++){
                    if(buffer[i] == "#".getBytes()[0]){
                        mHandler.obtainMessage(1, begin, i, buffer).sendToTarget();
                        begin = i + 1;
                        if(i == bytes - 1){
                            bytes = 0;
                            begin = 0;
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                break;
            }
        }

    }

    public void fallback(){

        try {

            BluetoothDevice btDevice = mBluetoothAdapter.getRemoteDevice(deviceAdress);
            bluetoothSocket = btDevice.createRfcommSocketToServiceRecord(UUID.fromString(myUUID));

            if (bluetoothSocket != null)
                bluetoothSocket.connect();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}

package unifor.nati.arduinon;

import android.os.Handler;
import android.os.Message;

/**
 * Created by Daniel on 27/01/2016.
 */
public class Handle_Singleton {

    private static Handle_Singleton mInstance = null;

    private Handle_Singleton() {

    }

    public static Handle_Singleton getInstance() {
        if (mInstance == null) {
            mInstance = new Handle_Singleton();
        }
        return mInstance;
    }

    private final Handler mHandler = new Handler(){
        @Override

        public void handleMessage(Message msg){
            byte[] writeBuf = (byte[]) msg.obj;
            int begin = (int)msg.arg1;
            int end = (int)msg.arg2;

            switch(msg.what) {
                case 1:
                    writeMessage = new String(writeBuf);
                    writeMessage = writeMessage.substring(begin, end);

                    break;
            }
        }
    };

    public Handler getHandler() {
        return mHandler;
    }

    private String writeMessage;

    public String getWriteMessage() {
        return writeMessage;
    }

    public void setWriteMessage(String writeMessage) {
        this.writeMessage = writeMessage;
    }



}
